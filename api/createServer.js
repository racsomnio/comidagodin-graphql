const { GraphQLServer } = require('graphql-yoga');
const typeDefs = require('../graphqlSchema');
const Query = require('../resolvers/Query');
const Mutation = require('../resolvers/Mutation');

//create graphql yoga server
function createServer(){
    return new GraphQLServer({ 
        typeDefs, 
        resolvers: {
            Owner: {
                __resolveType: owner => {
                    if(owner.isGood && owner.email){
                        return "User"
                    }
        
                    if(owner.phone){
                        return "Store";
                    }
                }
            },
            Query,
            Mutation,        },
        context: req => ({ ...req })
    });
}

module.exports = createServer;