const nodemailer = require('nodemailer');
const emailme = require('./emailme.json')

const transport = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  secure: true,
  // service: 'Gmail',
  auth: {
    type: 'OAuth2',
    user: process.env.MAIL_USER,
    serviceClient: emailme.client_id,
    privateKey: emailme.private_key,
    // pass: process.env.MAIL_PASS,
  },
});

const makeANiceEmail = text => `
  <div className="email" style="
    border: 1px dashed #cccccc;
    padding: 20px;
    font-family: sans-serif;
    line-height: 2;
    font-size: 12px;
    background-color: #ffffff;
  ">
    ${text}

    <p>comidagodin.com</p>
  </div>
`;

exports.transport = transport;
exports.makeANiceEmail = makeANiceEmail;
