const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const storeSchema = new Schema({
    rights: {
        type: [String],
        enum: ["FREE", "PLAN_LITE", "PLAN_LITE_BILINGUAL", "PLAN_PREMIUM", "EDITOR" ],
        default: ['FREE']
    },
    name : {
        type: String,
        required: true,
    },
    typeOfBusiness : [{
        type: String,
    }],
    location: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [{
            type: Number
        }],
        address: {
            type: String        
        }
    },
    email:{
        type: String,
        required: true,
        unique: true,
    },
    phone:[{
        number: {
            type: String,
            required: true,
            unique: true
        },
        whatsapp: {
            type: Boolean,
            default: false
        }
    }],
    wa_orders: {
        type: Boolean,
        default: false
    },
    
    password:{
        type: String,
        required: true,
    },
    createdDate:{
        type: Date,
        default: Date.now,
    },

    cover_img: [{
        secure_url: {
            type: String
        },
        public_id: {
            type: String
        },
        format: {
            type: String
        },
    }],
    profile_img: [{
        secure_url: {
            type: String
        },
        public_id: {
            type: String
        },
        format: {
            type: String
        },
    }],
    message: {
        type: String
    },
    speciality: [{
        type: String
    }],
    hours: [{
        from: {
            type: String,
        },
        to: {
            type: String,
        },
        open_hr: {
            type: String,
        },
        open_min: {
            type: String,
        },
        open_ampm: {
            type: String,
        },
        close_hr: {
            type: String,
        },
        close_min: {
            type: String,
        },
        close_ampm: {
            type: String,
        },
    }],
    social: {
        facebook: {
            type: String
        },
        instagram: {
            type: String
        },
        twitter: {
            type: String
        },
        pinterest: {
            type: String
        },
        website: {
            type: String
        },
        reservation: {
            type: String
        }
    },
    food_type: {
        code: {
            type: String
        },
        label: {
            type: String
        },
        phone: {
            type: String
        },
        type: {
            type: String
        }
    },
    delivery: {
        type: Boolean,
        default: false
    },
    takeaway: {
        type: Boolean,
        default: false
    },

    only_cash: {
        type: Boolean,
        default: false
    },

    card: {
        type: Boolean,
        default: false
    },

    open_tv: {
        type: Boolean,
        default: false
    },

    cable: {
        type: Boolean,
        default: false
    },

    ppv: {
        type: Boolean,
        default: false
    },

    speak_english: {
        type: Boolean,
        default: false
    },

    english_menu: {
        type: Boolean,
        default: false
    },

    vegan_option: {
        type: Boolean,
        default: false
    },

    vegetarian_option: {
        type: Boolean,
        default: false
    },

    no_gluten_option: {
        type: Boolean,
        default: false
    },

    no_shellfish_option: {
        type: Boolean,
        default: false
    },

    reservations: {
        type: Boolean,
        default: false
    },

    live_music: {
        type: Boolean,
        default: false
    },

    dancing: {
        type: Boolean,
        default: false
    },

    rooftop: {
        type: Boolean,
        default: false
    },

    terrace: {
        type: Boolean,
        default: false
    },

    garden: {
        type: Boolean,
        default: false
    },

    smoking_area: {
        type: Boolean,
        default: false
    },

    kids_menu: {
        type: Boolean,
        default: false
    },

    kids_area: {
        type: Boolean,
        default: false
    },

    wifi: {
        type: Boolean,
        default: false
    },

    private_room: {
        type: Boolean,
        default: false
    },

    parking: {
        type: Boolean,
        default: false
    },

    valet_parking: {
        type: Boolean,
        default: false
    },

    pet_friendly: {
        type: Boolean,
        default: false
    },

    catering: {
        type: Boolean,
        default: false
    },

    gay: {
        type: Boolean,
        default: false
    },

    has_breakfast: {
        type: Boolean,
        default: false
    },

    has_lunch_special: {
        type: Boolean,
        default: false 
    },

    has_healthy_food : {
        type: Boolean,
        default: false 
    },
    
    has_alcohol: {
        type: Boolean,
        default: false 
    },

    has_billiard: {
        type: Boolean,
        default: false
    },

    has_foosball : {
        type: Boolean,
        default: false
    },

    has_pool: {
        type: Boolean,
        default: false
    },

    has_karaoke: {
        type: Boolean,
        default: false
    },

    big_groups: {
        type: Boolean,
        default: false
    },

    etiquette: {
        type: String,
        default: 'Casual'
    },

    min_price: {
        type: Number
    },

    menus: [{
        type: Schema.Types.ObjectId,
        ref: 'Menu',
    }],
    
    full_menu: {
        type: Schema.Types.ObjectId,
        ref: 'FullMenu',
    },
    
    stored_pics: [{
        type: String
    }],

    resetToken: {
        type: String
    },
    resetTokenExpiry: {
        type: Date
    },

    slug: String,
    stripeSessionId:{
        type: String
    },
    stripeCustomerId:{
        type: String
    },
    subscriptionId:{
        type: String
    },
    productId:{
        type: String
    },
    priceId:{
        type: String
    },
});

storeSchema.pre('save', async function(next) {
    if (!this.isModified('name')) {
      next(); // skip it
      return; // stop this function from running
    }

    const removedAccents = this.name.toLowerCase().replace(/[áéíóúüñ ]/g, x => {
        switch(x) {
            case "á":
                return "a"
                break;
            case "é":
                return "e"
                break;
            case "í":
                return "i"
                break;
            case "ó":
                return "o"
                break;
            case "ú":
                return "u"
                break;
            case "ü":
                return "u"
                break;
            case "ñ":
                return "n"
                break;
            case " ":
                return "-"
                break;
        }
    })

    this.slug = removedAccents;
    // find other stores that have a slug of wes, store-2, store-3
    const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
    const storesWithSlug = await this.constructor.find({ slug: slugRegEx });
    if (storesWithSlug.length) {
      this.slug = `${this.slug}-${storesWithSlug.length + 1}`;
    }
    next();
});

storeSchema.pre('save', function(next){
    if(!this.isModified('password')){
        return next();
    } 
    bcrypt.genSalt(10, (err, salt) => {
        if(err) return next(err);
        bcrypt.hash(this.password, salt, (err, hash) => {
            if(err) return next(err);
            this.password = hash;
            next();
        })
    })
});

mongoose.set('useCreateIndex', true); // needed to avoid deprecation warning
storeSchema.index({
    location: '2dsphere' // needed for geoNear
})

module.exports = mongoose.model('Store', storeSchema)