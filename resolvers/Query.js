const User = require('../mongoModels/User');
const Store = require('../mongoModels/Store');
const Menu = require('../mongoModels/Menu');

const Query = {
  getCurrentUser: async (parent, { _id }, context, info) => {
    const { userId } = context.request;
    // Check if there is a user ID
    if(!userId){
      return null;
    }
    
    let user;
    user = await Store.findOne({ _id: userId });

    if(!user){
      user = await User.findOne({ _id: userId });
    }
    return user;
  },

  getMyStore: async (root, {_id}, context) => {
    const { userId } = context.request;
    // Check if there is a user ID
    if(!userId){
      return null;
    }
    const storeId = _id || userId;
    if(_id) {
      const isEditor = await Store.findOne({ _id:userId, rights: "EDITOR"})
      if(!isEditor) throw new Error('Sólo personal autorizadow');
    }
    const store = await Store.findOne({ _id: storeId });
    return store;
  },

  getStore: async (root, {_id}, context) => {
    const store = await Store.findOne({ _id });
    return store;
  },

  getStoreBySlug: async (root, {slug}, context) => {
    const store = await Store.findOne({ slug }).populate({
      path: 'full_menu',
      model: 'FullMenu',
    });

    if(!store) throw new Error("Parece que te has perdido en los pasillos")
    
    return store;
  },

  getAllStores: async (root, args, context) => {
    const { userId } = context.request;
    // Check if there is a user ID
    if(!userId){
      return null;
    }

    const isAdmin = await Store.findOne({ _id:userId, rights: "EDITOR"})
    if(!isAdmin){
      return null;
    }

    const stores = await Store.find({}).sort( { createdDate: -1 } ).populate({
        path: 'menus',
        model: 'Menu',
    })
    return stores;
  },

  getStoresByGeo: async (root, { coordinates }, context) => {
    const coords = coordinates.split(',').map(Number);
    const today = new Date().getDay().toString();

    // return menus;
    const all = await Store.aggregate([
        {
          $geoNear: {
            near: { type: "Point", coordinates: coords },
            distanceField: "distance",
            maxDistance: 50000, // change this to 2kms
            spherical: true,
            // query: { meal, repeat: {$all: [today]} }
          }
        },
        // { $limit: 5 }
    ])
    // .exec().then((docs) => {
    //     // console.log('Docs: ' + JSON.stringify(docs));
    //     const res = Store.populate(docs, {path:'owner', model:'Store'});
    //     return res;
    // });

    return all;
  },

  getMenus: async (root, { coordinates, meal }, context) => {
    const coords = coordinates.split(',').map(Number);
    const today = new Date().getDay().toString();

    // return menus;
    const allFood = await Menu.aggregate([
        {
          $geoNear: {
            near: { type: "Point", coordinates: coords },
            distanceField: "distance",
            maxDistance: 20000,
            spherical: true,
            query: { meal, repeat: {$all: [today]} }
          }
        },
        // { $limit: 5 }
    ]).exec().then((docs) => {
        // console.log('Docs: ' + JSON.stringify(docs));
        const res = Menu.populate(docs, {path:'owner', model:'Store'});
        return res;
    });

    return allFood;
  },

  getMenu: async (root, {_id}, context) => {
    const menu = await Menu.findOne({ _id }).populate({
        path: 'owner',
        model: 'Store',
    });
    if(!menu) throw new Error('Oops! Este menú ha expirado :(');
    return menu;
  },

  getMyMenus: async (root, {_id}, context) => {
    const { userId } = context.request;
    // Check if there is a user ID
    if(!userId){
      return null;
    }

    const menu = await Store.findOne({ _id: userId }).populate({
        path: 'menus',
        model: 'Menu',
    });
    
    return menu.menus;
  },
};

module.exports = Query;

